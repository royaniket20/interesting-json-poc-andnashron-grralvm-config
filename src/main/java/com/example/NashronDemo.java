package com.example;


import jdk.nashorn.api.scripting.ClassFilter;
import jdk.nashorn.api.scripting.NashornScriptEngineFactory;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import com.oracle.truffle.js.scriptengine.GraalJSScriptEngine;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Engine;
public class NashronDemo {

  public static void main(String[] args) throws ScriptException {
    NashronDemo nashronDemo = new NashronDemo();
    ScriptEngine scriptEngine = nashronDemo.NashronEvaluation();
    nashronDemo.evalScript(scriptEngine);
    scriptEngine = nashronDemo.GraalVmEvaluation();
    nashronDemo.evalScript(scriptEngine);
    scriptEngine = nashronDemo.GraalVmEvaluation();
    nashronDemo.thirdExample(scriptEngine);
  }

  public void evalScript(ScriptEngine scriptEngine){
    // Create Java array with list of cities
    HelloWorld helloWorld = new HelloWorld();
    helloWorld.name = "Aniket Roy";
    String cities[] = { "London", "NewYork", "Sydney", "Bangalore",
        "Chennai", "Mumbai" };

    // Create script which accessing Java object
    String script = "var index; " + "var cities = citiesArray;" +  "print(myname.name+'----'+myname.call());"
        + "for (index in cities) { " + "print(cities[index]);" + "}";
    // Add Java object to script engine
    scriptEngine.put("citiesArray", cities);
    scriptEngine.put("myname", helloWorld);
    try {
      // Evaluate  script using script engine
      scriptEngine.eval(script);
    } catch (ScriptException exception) {
      exception.printStackTrace();
    }
  }

  public void thirdExample(ScriptEngine scriptEngine) throws ScriptException {
    Bindings bindings = scriptEngine.getBindings(ScriptContext.ENGINE_SCOPE);
    bindings.put("javaObj", new Object());
    scriptEngine.eval("print(javaObj instanceof Java.type('java.lang.Object'));"); // it will not work without
    // allowHostAccess and allowHostClassLookup
  }

  public ScriptEngine NashronEvaluation() throws ScriptException {
    NashornScriptEngineFactory factory = new NashornScriptEngineFactory();
    ScriptEngine engine = null;
     engine = factory.getScriptEngine(new ClassFilter() {
      @Override
      public boolean exposeToScripts(String className) {
        return false;
      }
    });
    if (engine == null) {
      engine = new ScriptEngineManager().getEngineByName("javascript");
    }
    if (engine == null) {
      throw new ScriptException("Unable to obtain ScriptEngine from NashornScriptEngineFactory or ScriptEngineManager");
    }
    System.out.println(" Engine - "+ engine.toString());
    return engine;
  }


  public ScriptEngine GraalVmEvaluation() throws ScriptException {
    ScriptEngine engine =null;
    engine = GraalJSScriptEngine.create(
        Engine.newBuilder().option("engine.WarnInterpreterOnly", "false").build(),
        Context.newBuilder("js")
           //.allowHostClassLookup(s->false)
            .allowHostClassLookup(s->true).allowHostAccess(true)
    );

    if (engine == null) {
      engine = new ScriptEngineManager().getEngineByName("javascript");
    }
    if (engine == null) {
      throw new ScriptException("Unable to obtain ScriptEngine from GraalJSScriptEngine or ScriptEngineManager");
    }
    System.out.println(" Engine - "+ engine.toString());
    return engine;
  }
}
