package com.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.internal.filter.ValueNodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class JsonPathDemo {
  private static final String SPLIT_REGEX = "\\.";
  private static final String ARRAY_BEGIN = "[";
  private static final String ARRAY_END = "]";
  static org.slf4j.Logger log = LoggerFactory.getLogger(JsonPathDemo.class);
  public static void main(String[] args) throws IOException {
    Map<String, Object> jsonValues = new HashMap<>();
    jsonValues.put("data.employeeDetails[0].firstName" , "Aniket");
    jsonValues.put("data.employeeDetails[0].address[0].street" , "Kolkata");
    jsonValues.put("data.employeeDetails[0].address[0].colony.pin" , 711303);
    jsonValues.put("data.employeeDetails[0].testing.finding.address[0].colony.roll", 22);
    jsonValues.put("test[0].employeeDetails[0].testing.finding.address[0].colony.roll", 22);
    jsonValues.put("data.employeeDetails[0].address[0].colony.isSecured" , false);
    ObjectMapper objectMapper = new ObjectMapper();
    String jsonStr = "{\"test\" : [   [ [] ] , []  ],\"data\":{\"employeeDetails\":[{\"testing\":{\"finding" +
        "\":{\"address" +
        "\":[{\"colony\":{\"name\":{}}}]}}}]}}";
    JsonNode node = objectMapper.readTree(jsonStr);
     new JsonPathDemo().updateoutputJsonbasedOnRequestPathRequirement(jsonValues,node , objectMapper);
  }

  public JsonNode updateoutputJsonbasedOnRequestPathRequirement(Map<String, Object> setDataMap,
                                                                 JsonNode jsonNode, ObjectMapper objectMapper) throws IOException {
    log.error(" Checking for Request Path and Json state to make Output Json compatible for path Update ");
    JsonNode node = jsonNode.deepCopy();
    log.error("Given Json Node - {}", objectMapper.writeValueAsString(node));
    List<String> jsonPaths = setDataMap.entrySet().stream().map(item->item.getKey()).collect(Collectors.toList());
    log.error("Given Json Paths to prepare missing Node - {}",jsonPaths);
    //Will Do Json preparation for each Given Json Path
    for (String  item :jsonPaths) {
      log.error("Given Path = {}" , item);
      List<String> tokens = Arrays.asList(item.split(SPLIT_REGEX));
      log.error(" Given Tokens - {}" , tokens); //a.b.c.d[0].e.f[0] == {a,b,c,d[0],e,f[0]}
      //For each Json path - That is broken into Smaller tokens and use them to prepare missing nodes
      JsonNode current = node;  //First we will start with Initial Node given to the method - then traverse token by
      // token and Update missing Node and Update current to that Node
      for (String token : tokens) {
        log.error("processing current token - {}",token);
        //When a token represent an Array Node like  d[0]
        if (token.endsWith(ARRAY_END)) {
          //Stripping Out the index information - as we inherently assume to be on 0th index
          token = token.substring(0, token.indexOf(ARRAY_BEGIN));
          log.error("Array Node = {}" , token);

          //Preparing a Json Node which Looks like this -  { name: [] }
          JsonNode emptyJsonNode = objectMapper.createObjectNode(); // {}
          ArrayNode blankArrayNode = objectMapper.createArrayNode();// []
          ((ObjectNode) emptyJsonNode).put(token, blankArrayNode);  // { name: [] }

          //Where we are going to add this missing node -that current node can itself be an array or Object Node
          //If current node is  array Node
          if (current.isArray()) {
            ArrayNode nodeToBeManipulated = (ArrayNode) current;
            if (nodeToBeManipulated.get(0) == null) {
              /**
               *  {                     {
               *    data : []   ===>     data : [ { name: [] } ]
               *  }                     }
               */
              nodeToBeManipulated.insert(0, emptyJsonNode);
            } else {
              if (nodeToBeManipulated.get(0).isArray()) {
                /**SPECIAL CASES - MAY NOT RELEVANT TO PLOR RIGHT NOW
                 *  {                      {
                 *    data : [[]]   ===>     data : [ { name: [] } , [] ]
                 *  }                      }
                 */
                nodeToBeManipulated.insert(0, emptyJsonNode);
              } else {
                /**
                 *  {                                             {
                 *    data : [{ k1: v1 , k2: v2 , ... }]   ===>     data : [ { name: []  , k1: v1 , k2: v2 , ...}  ]
                 *  }                                             }
                 */
                objectMapper.readerForUpdating(nodeToBeManipulated.get(0)).readValue(emptyJsonNode);
              }
            }
            //If current node is  Object Node
          } else {
            //If the token path is missing then just add a new token path
            /**suppose token = data.element
             *  {                     {
             *    data : null   ===>     data : {element : []}
             *  }                     }
             */
            JsonNode nodeToBeManipulated = current;
            if (nodeToBeManipulated.path(token).isMissingNode()) {
              ((ObjectNode) nodeToBeManipulated).put(token, blankArrayNode);
            }
          }

          //When a token represent an Object/Primitive Node like a,b,c
        } else {
          log.error("Object/Primitive Node = {}" , token);
          //Preparing a Json Node which Looks like this -  { name: {} }
          JsonNode emptyJsonNode = objectMapper.createObjectNode(); // {}
          ((ObjectNode) emptyJsonNode).put(token, objectMapper.createObjectNode()); // { name : {} }
          //Where we are going to add this missing node -that current node can itself be an array or Object Node
          //If current no is  array Node
          if (current.isArray()) {
            ArrayNode nodeToBeManipulated = (ArrayNode) current;
            if (nodeToBeManipulated.get(0) == null) {
              /**
               *  {                     {
               *    data : []   ===>     data : [ { name : {} } ]
               *  }                     }
               */
              nodeToBeManipulated.insert(0, emptyJsonNode);
            } else {
              if (nodeToBeManipulated.get(0).isArray()) {
                /** SPECIAL CASES - MAY NOT RELEVANT TO PLOR RIGHT NOW
                 *  {                       {
                 *    data : [[]]   ===>     data : [ { name : {} } , [] ]
                 *  }                       }
                 */
                nodeToBeManipulated.insert(0, emptyJsonNode);
              } else {
                /**
                 *  {                                             {
                 *    data : [{ k1: v1 , k2: v2 , ... }]   ===>     data : [ { name : {}  , k1: v1 , k2: v2 , ...}  ]
                 *  }                                             }
                 */
                objectMapper.readerForUpdating(nodeToBeManipulated.get(0)).readValue(emptyJsonNode);
              }
            }
            //If current node is  Object Node
          } else {
            //If the token path is missing then just add a new token path
            /**suppose token = data.element
             *  {                     {
             *    data : null   ===>     data : {element : {}}
             *  }                     }
             */
            JsonNode nodeToBeManipulated = current;
            if (nodeToBeManipulated.path(token).isMissingNode()) {
              ((ObjectNode) nodeToBeManipulated).put(token, objectMapper.createObjectNode());
            }
          }
        }


        log.error("Now Update the current node to newly added/traversed Node so that future elements can be traversed");
        //if current Node from where we started is an array Node - oviously we have updated the 0th element with our
        // current token so go to that first array element and go to the token path
        /**
         * we are at data node --> need to go to data[0].name
         *  {
         *    data : [
         *    { name : {} }
         *    ]
         *  }
         */
        if (current.isArray()) {
          current = current.get(0).path(token);

          //if current Node from where we started is an Object Node - obviously we have added field with our
          // current token so go to that  token path
          /**
           * we are at data node --> need to go to data.name node
           *  {
           *    data : { name : {} }
           *  }
           */
        } else {
          current = current.path(token);
        }
      }

    }
    log.error("Updated Json Node - {}", objectMapper.writeValueAsString(node));
    return  node;
  }
}
