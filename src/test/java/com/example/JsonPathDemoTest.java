package com.example;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class JsonPathDemoTest {

  @Test
  public void  updateoutputJsonbasedOnRequestPathRequirementTest () throws Exception {
    Map<String, Object> setDataMap = new HashMap<>();
    setDataMap.put("data.employeeDetails[0].firstName" , null);
    setDataMap.put("data.employeeDetails[0].address[0].street" ,null);
    setDataMap.put("data.employeeDetails[0].address[0].colony.pin" , null);
    setDataMap.put("data.employeeDetails[0].testing.finding.address[0].colony.roll", null);
    setDataMap.put("test[0].employeeDetails[0].testing.finding.address[0].colony.roll", null);
    setDataMap.put("data.employeeDetails[0].address[0].colony.isSecured" , null);
    String jsonStr = "{\"test\" : [   [ [] ] , []  ],\"data\":{\"employeeDetails\":[{\"testing\":{\"finding" +
        "\":{\"address" +
        "\":[{\"colony\":{\"name\":{}}}]}}}]}}";
    String addrObj = "{ \"addr1\" : \"floor 1\" , \"addr2\" : \"floor2\" }";
    ObjectMapper tempMapper =  new ObjectMapper();
    JsonNode jsonNode = tempMapper.readTree(jsonStr);
    JsonPathDemo jsonPathDemo = new JsonPathDemo();
    JsonNode result =  jsonPathDemo.updateoutputJsonbasedOnRequestPathRequirement( setDataMap, jsonNode ,tempMapper);
    String finalJson = tempMapper.writeValueAsString(result);
    List<String> jsonPaths =
        setDataMap.entrySet().stream().map(item->item.getKey()).map(data-> "$."+data).collect(Collectors.toList());
    Object document = Configuration.defaultConfiguration().jsonProvider().parse(finalJson);
    jsonPaths.forEach(path->{
      Object value = JsonPath.read(document, path);
      Assert.assertNotNull(value);
    });
    String newJson = JsonPath.parse(finalJson).set("$.data.employeeDetails[0].testing.finding.address[0].colony.roll", Arrays.asList(1,2,3,4)).jsonString();
    newJson = JsonPath.parse(newJson).set("$.data.employeeDetails[0].address[0].colony.pin", 711303).jsonString();
    newJson =
        JsonPath.parse(newJson).set("$.data.employeeDetails[0].address[0].street", addrObj).jsonString();
    System.out.println(newJson);
    Object documentUpdated = Configuration.defaultConfiguration().jsonProvider().parse(newJson);
    Object value = JsonPath.read(documentUpdated, "$.data.employeeDetails[0].testing.finding.address[0].colony.roll");
    Assert.assertTrue(value.equals(Arrays.asList(1,2,3,4)));
    value = JsonPath.read(documentUpdated, "$.data.employeeDetails[0].address[0].colony.pin");
    Assert.assertTrue(value.equals(711303));
    value = JsonPath.read(documentUpdated, "$.data.employeeDetails[0].address[0].street");
    Assert.assertTrue(value.equals(addrObj));
  }

}
